﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.VisualBasic.FileIO;
using Accord.Neuro;
using Accord.Neuro.Learning;

using WebApplication.Controllers;
using System.Threading.Tasks;

namespace WebApplication.App_Start
{
    public class Neuro {

        double[][] input;
        double[][] output;

        ActivationNetwork net;


        public Neuro(string pathToData)
        {
            read(pathToData, 0);
            
            learn();
        }

        void read(string path, int parametr)
        {
            using (TextFieldParser parser = new TextFieldParser(path))
            {
                List<double[]> inp = new List<double[]>();
                List<double[]> outp = new List<double[]>();
                parser.Delimiters = new string[] { "," };
                while (true)
                {
                    List<double> inpCurr = new List<double>();
                    double[] outCurr = new double[5];

                    string[] parts = parser.ReadFields();
                    if (parts == null)
                    {
                        break;
                    }
                    for (int i = 0; i < parts.Length - 5; i++)
                    {
                        inpCurr.Add(double.Parse(parts[i]));
                    }
                    for (int i = 0; i < 5; i++)
                    {
                        outCurr[i] = double.Parse(parts[parts.Length - (5 - i)]);
                    }
                    inp.Add(inpCurr.ToArray());
                    outp.Add(outCurr);
                }
                input = inp.ToArray();
                output = outp.ToArray();
            }
        }
        void learn()
        {
            int inpCount = 12;
            int[] layers = { 12, 5 };
            net = new ActivationNetwork(new SigmoidFunction(2), inpCount, layers);
            BackPropagationLearning teach = new BackPropagationLearning(net);
            teach.LearningRate = 0.5;
            teach.Momentum = 0.6;
            for (int ep = 0; ep < 3000; ep++)
            {
                var error = teach.RunEpoch(input, output);
                if (ep % 100 == 0) Console.WriteLine("{0}: error {1}", ep, error);
            }
        }

        public async Task<double[]> calc(string text)
        {
            double[] input = await VectorController.textProcessing(text);
            return net.Compute(input);
        }
    }
}