﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using WebApplication.App_Start;
using Word2vec.Tools;

namespace WebApplication
{
    public static class WebApiConfig
    {
        public static Vocabulary vocabulary;
        public static HttpClient httpClient = new HttpClient();
        public static EmotionalDictionary emoDict = new EmotionalDictionary(HttpContext.Current.Server.MapPath("~/App_Data/dict.txt"));
        public static Neuro neuroNetwork = new Neuro(HttpContext.Current.Server.MapPath("~/App_Data/dataToLearn.csv"));

        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Formatters.JsonFormatter.SupportedMediaTypes
            .Add(new MediaTypeHeaderValue("text/html"));
            init();

        }
        public static void init()
        {
            string path = HttpContext.Current.Server.MapPath("~/App_Data/vectors.bin");
            vocabulary = new Word2VecBinaryReader().Read(path);
        }
    }
}
