﻿using System;
using System.Collections.Generic;

using System.IO;

namespace WebApplication.App_Start
{
    public class EmotionalDictionary
    {
        Dictionary<string, int[]> dict;
        public EmotionalDictionary(string path)
        {
            dict = new Dictionary<string, int[]>();
          
            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                        string[] line = sr.ReadLine().Split(null);
                        string word = line[0];
                        string emo = line[1];
                        int num = 0;
                        int.TryParse(line[2], out num);
                        

                        if (!dict.ContainsKey(word))
                        {
                            dict.Add(word, new int[10]);
                        }
                        
                        switch (emo)
                        {
                            case "anger": dict[word][0] = num; break;
                            case "anticipation": dict[word][1] = num; break;
                            case "disgust": dict[word][2] = num; break;
                            case "fear": dict[word][3] = num; break;
                            case "joy": dict[word][4] = num; break;
                            case "negative": dict[word][5] = num; break;
                            case "positive": dict[word][6] = num; break;
                            case "sadness": dict[word][7] = num; break;
                            case "surprise": dict[word][8] = num; break;
                            case "trust": dict[word][9] = num; break;
                        }
                    }
                }
        }

        public double[] getEmotions(string word)
        {
            double[] emotions = new double[10];
            for (int i = 0; i < 10; i++)
            {
               emotions [i] = dict[word][i];
            }
            return emotions;
        }

        public bool exists(string word)
        {
            return dict.ContainsKey(word);
        }
    }
}