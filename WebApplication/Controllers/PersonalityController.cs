﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebApplication.Controllers
{
    public class PersonalityController : ApiController
    {
        public async Task<double[]> Post([FromBody] dynamic req)
        {
            double[] outArray = new double[5];
            string text = (string)req.text;
            if (WebApiConfig.neuroNetwork != null)
            {
                outArray = await WebApiConfig.neuroNetwork.calc((string)req.text);
            }
            return outArray;
        }
    }
}
