﻿using System.Web.Http;

namespace WebApplication.Controllers
{
    public class WordAnalysisController : ApiController
    {
        public double[,] Post([FromBody] dynamic req)
        {
           // List<string> debug = new List<string>();
            string[] wordsArray1 = ((string)req.words1).Split(',');
            string[] wordsArray2 = ((string)req.words2).Split(',');
          //  debug.Add(words1+" "+words2+" "+wordsArray1.Length+" "+wordsArray2.Length);

            double[,] resultArray = new double[wordsArray1.Length, wordsArray2.Length];
            for (int i = 0; i < wordsArray1.Length; ++i)
            {
                for (int j = 0; j < wordsArray2.Length; ++j)
                {
                    if (!(WebApiConfig.vocabulary.ContainsWord(wordsArray1[i]) && WebApiConfig.vocabulary.ContainsWord(wordsArray2[j])))
                    {
                        //debug.Add(WebApiConfig.vocabulary.ContainsWord(wordsArray1[i])+ " not exists "+ WebApiConfig.vocabulary.ContainsWord(wordsArray2[j]));
                        continue;
                    }
                    var word1Representation = WebApiConfig.vocabulary[wordsArray1[i]];
                    var word2Representation = WebApiConfig.vocabulary[wordsArray2[j]];
                    double distance = word1Representation.GetCosineDistanceTo(word2Representation).DistanceValue;

                    resultArray[i ,j] = distance;// distance;
                   // return distance;
                }
            }
            return resultArray;
        }        
    }
}
