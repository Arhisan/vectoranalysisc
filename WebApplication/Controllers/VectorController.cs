﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace WebApplication.Controllers
{
    public class VectorController : ApiController
    {
        public static async Task<double[]> textProcessing(string text)
        {
            if (text == null)
            {
                return new double[12];
            }
            double[] resultArray = new double[12];

            Dictionary<string, int> uniqueWords = new Dictionary<string, int>();
            double sentiment = 0;
            int wordCount = 0;
            int wordsLongerThan6Count = 0;
            double[] emotionalVect = new double[10];

            string[] sentences = text.Split(new[] { '.', '?', '!' }, StringSplitOptions.RemoveEmptyEntries);
            if (sentences.Length <= 0)
            {
                return new double[12];
            }
            for (int i = 0; i < sentences.Length; i++)
            {
                string[] words = sentences[i].Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                wordCount += words.Length;

                for (int j = 0; j < words.Length; j++)
                {
                    if (words[j].Length > 6) wordsLongerThan6Count++;

                    if (WebApiConfig.emoDict.exists(words[j]))
                    {
                        double[] emoArray = WebApiConfig.emoDict.getEmotions(words[j]);
                        for (int emo = 0; emo < 10; emo++)
                        {
                            emotionalVect[emo] += emoArray[emo];
                        }
                    }
                    if (!uniqueWords.ContainsKey(words[j])) uniqueWords.Add(words[j], 0);
                    uniqueWords[words[j]]++;
                }
            }
            double wordsPerSentence = (double)wordCount / sentences.Length;
            double wordsLongerThan6Rate = (double)wordsLongerThan6Count / wordCount;
            double uniqueWordsRate = (double)uniqueWords.Count / wordCount;
            // Normalizing emotions vector
            //for (int i = 0; i < emotionalVect.Length; i++)
            //{
            //   emotionalVect[i] /= wordCount;
            //}

            // Output
            //   sentiment = await GetSentimentAsync(text);
            //   resultArray[0] = sentiment;
            double sum = 0;
            for (int i = 0; i < 10; i++)
            {
                sum += emotionalVect[i];
            }
            for (int i = 0; i < 10; i++)
            {
                resultArray[i] = Math.Round(emotionalVect[i] / sum, 6);
            }
            // resultArray[10] = wordCount;
            // resultArray[11] = wordsPerSentence;
            resultArray[10] = Math.Round(uniqueWordsRate, 6);
            resultArray[11] = Math.Round(wordsLongerThan6Rate, 6);
            return resultArray;
        }

        public async Task<double[]> Post([FromBody]dynamic req)
        {
            if (req.text == null)
            {
                return new double[12];
            }
            string text = req.text;
            return await textProcessing(text);
        }


        async Task<double> GetSentimentAsync(string text)
        {
            WebApiConfig.httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "bdcdfabb7e034f088cfd6615580d5e99");
            var uri = "https://westus.api.cognitive.microsoft.com/text/analytics/v2.0/sentiment";
            HttpResponseMessage response;
            string requestString = "{'documents':[{'id':'1','text':'" + text.Replace("\'","\\\'").Replace("\"","\\\"") + "'}]}";
            byte[] byteData = Encoding.UTF8.GetBytes(requestString);
            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                response = await WebApiConfig.httpClient.PostAsync(uri, content);
                string textResponce = await response.Content.ReadAsStringAsync();
                WebApiConfig.httpClient.DefaultRequestHeaders.Clear();
                var jss = new JavaScriptSerializer();
                dynamic dict = null;
                // string json = jss.Serialize(dict["documents"][0]["keyPhrases"]);
                try
                {
                    dict = jss.Deserialize<Dictionary<string, dynamic>>(textResponce);
                    return (double)dict["documents"][0]["score"];
                }
                catch (Exception e)
                {
                    throw new Exception(jss.Serialize(textResponce));
                }
            }
        }
    }
}
