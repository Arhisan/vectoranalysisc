﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace WebApplication.Controllers
{
    public class KeyWordsController : ApiController
    {
        public async Task<dynamic> Post([FromBody] dynamic req)
        {
            string sentence = req.text;
            if (sentence == null)
            {
                return null;
            }
            sentence = await translate(sentence);
            WebApiConfig.httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "bdcdfabb7e034f088cfd6615580d5e99");
            var uri = "https://westus.api.cognitive.microsoft.com/text/analytics/v2.0/keyPhrases";
            HttpResponseMessage response;
            string requestString = "{'documents':[{'id':'1','text':'" + sentence.Replace("\'", "\\\'").Replace("\"", "\\\"") + "'}]}";
            byte[] byteData = Encoding.UTF8.GetBytes(requestString);
            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                response = await WebApiConfig.httpClient.PostAsync(uri, content);
                string textResponce = await response.Content.ReadAsStringAsync();
                WebApiConfig.httpClient.DefaultRequestHeaders.Clear();
                var jss = new JavaScriptSerializer();
                var dict = jss.Deserialize<Dictionary<string, dynamic>>(textResponce);
                try
                {
                    return dict["documents"][0]["keyPhrases"];
                }
                catch (Exception e)
                {
                    throw new Exception(jss.Serialize(dict));
                }
            }
        }

        public async Task<string> translate(string inputText)
        {
            string authSubToken = "991591e900c74df891d9624ff380aaf6";
            string authToken = "";
            try
            {
                authToken = await GetAccessToken(authSubToken);
            }
            catch
            {
                return inputText;
            }
                   
            string to = "en";
            string uri = "http://api.microsofttranslator.com/v2/Http.svc/Translate?text=" + inputText + "&to=" + to;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add("Authorization", authToken);
            WebResponse response = null;
            string result = "";
            try
            {
                response = httpWebRequest.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    DataContractSerializer dcs = new DataContractSerializer(Type.GetType("System.String"));
                    result = (string)dcs.ReadObject(stream);
                }

            }
            catch
            {
                //throw;
                return inputText;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
            }
            return result;
        }

        static async Task<string> GetAccessToken(string authKey)
        {
            WebApiConfig.httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", authKey);
            var uri = "https://api.cognitive.microsoft.com/sts/v1.0/issueToken";
            HttpResponseMessage response;
            byte[] byteData = Encoding.UTF8.GetBytes("");

            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                response = await WebApiConfig.httpClient.PostAsync(uri, content);
                string textResponce = await response.Content.ReadAsStringAsync();
                WebApiConfig.httpClient.DefaultRequestHeaders.Clear();
                var jss = new JavaScriptSerializer();
                try
                {
                    return "Bearer "+textResponce;
                }
                catch (Exception e)
                {
                    throw new Exception("La probleme");
                }
            }
        }

    }       
}
