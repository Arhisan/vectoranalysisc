﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace WebApplication.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public async Task<dynamic> Post([FromBody]dynamic req)
        {
            var uri = "http://localhost:8083/post";
            HttpResponseMessage response;
            string requestString = "{'example':'"+req.text+"'}";
            byte[] byteData = Encoding.UTF8.GetBytes(requestString);
            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                response = await WebApiConfig.httpClient.PostAsync(uri, content);
                string textResponce = await response.Content.ReadAsStringAsync();
                var jss = new JavaScriptSerializer();
                var dict = jss.Deserialize<Dictionary<string, dynamic>>(textResponce);
                // System.Threading.Thread.Sleep(500);
                return dict;
            }
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
