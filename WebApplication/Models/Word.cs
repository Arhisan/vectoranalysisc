﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class Word
    {
        public string word { get; set; }
        public float[] vector { get; set; }
    }
}